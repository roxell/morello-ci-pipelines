#!/bin/bash

project_details=$(echo ${1} | base64 --decode)
echo $project_details | jq -c '.[]' | while read project_detail; 
do 
	PROJECT_PATH=$(echo $project_detail| jq -r ."project_path")
	SHA=$(echo $project_detail| jq -r ."sha") 
	PROJECT_BRANCH=$(echo $project_detail| jq -r ."branch")
	IID=$(echo $project_detail| jq -r ."iid")
	PROJECT_NAME=$(echo ${PROJECT_PATH} | cut -d'/' -f2-)
	if [[ ${PROJECT_NAME} =~ ^android* ]]; then
		PROJECT_NAME=$(echo "${PROJECT_NAME}" | cut -d'/' -f2-)
	fi
	echo "Patching $2 with project: ${PROJECT_NAME} and branch: ${PROJECT_BRANCH}"
        xmlstarlet edit --inplace \
          --update "//extend-project[@name=\"${PROJECT_NAME}\"]/@revision" \
          --value ${PROJECT_BRANCH} ${2}
        xmlstarlet edit --inplace \
          --update "//project[@name=\"${PROJECT_NAME}\"]/@revision" \
          --value ${PROJECT_BRANCH} ${2}
done
