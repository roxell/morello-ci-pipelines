#!/bin/sh

export LAVA_SERVER=${LAVA_SERVER:-https://lava.morello-project.org/}
export NET_ID="net-${BUILD_JOB_ID}-${RANDOM}"

${CI_PROJECT_DIR}/lava/submit_for_testing.py \
  --template-path ${CI_PROJECT_DIR}/lava/templates \
  --template-name ${LAVA_TEMPLATE_NAME} \
  --lava-server ${LAVA_SERVER} \
  --lava-token ${CI_LAVA_TOKEN}
