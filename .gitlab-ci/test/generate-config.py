import os
import sys

test_list = []
bionic_tests = []
static_tests = [
    "android-binder",
    "android-compartment",
    "android-device-tree",
    "android-dvfs",
    "android-logd",
    "android-multicore",
    "android-bionic",
]

if os.environ.get("USER_DEFINED_TESTS") is not None:
    default_tests = os.environ.get("USER_DEFINED_TESTS").strip("\"").strip("'").split()
else:
    default_tests = static_tests

for test in default_tests:
    if test == "android-bionic":
        bionic_tests = [
            "-string.:string_nofortify.",
            "string_nofortify.*-string_nofortify.strlcat_overread:string_nofortify.bcopy:string_nofortify.memmove",
            "string_nofortify.strlcat_overread:string_nofortify.bcopy:string_nofortify.memmove",
            "string.*-string.strlcat_overread:string.bcopy:string.memmove",
            "string.strlcat_overread:string.bcopy:string.memmove",
        ]
    elif test in static_tests:
        test_list.append(test)
    else:
        print(f"Dropping testsuite {test} from the list, not a valid suite")

if not bionic_tests and not test_list:
    print(f"WARNING: No valid testsuite have been passed")

# generate config-gitlab-ci-values.yml
original_stdout = sys.stdout
with open("config-values.yml", "w") as f:
    sys.stdout = f
    print("#@data/values")
    print("---")
    print("build_job_id:", os.environ.get("BUILD_JOB_ID"))
    print("services:", test_list)
    print("bionic:", bionic_tests)
    sys.stdout = original_stdout
