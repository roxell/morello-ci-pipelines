#!/bin/sh

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"

set -ex

rm -rf .repo/manifests
repo init --depth=1 --no-tags --no-clone-bundle \
  -u https://git.morello-project.org/morello/manifest.git \
  -b ${MANIFEST_BRANCH} -g bsp \
  --repo-rev=v2.16

repo selfupdate
repo version

repo sync -j8 --quiet --no-clone-bundle
repo manifest -r -o pinned-manifest.xml
cat pinned-manifest.xml

cd bsp
MACHINE=morello-fvp DISTRO=poky source conf/setup-environment-morello

# Get build stats to make sure that we use sstate properly
cat << EOF >> conf/auto.conf
INHERIT += "buildstats buildstats-summary"
EOF

# Make sure we don't use rm_work in CI slaves since they are non persistent build nodes
cat << EOF >> conf/auto.conf
INHERIT_remove = "rm_work"
EOF

# Write information to the target filesystem on /etc/build
cat << EOF >> conf/auto.conf
INHERIT += "image-buildinfo"
EOF

# Include extra packages to the image
cat << EOF >> conf/local.conf
CORE_IMAGE_EXTRA_INSTALL += "\\
    packagegroup-core-buildessential \\
    acpica \\
    ethtool \\
    fwts \\
    git \\
    haveged \\
    ltp \\
    python3 \\
    python3-misc \\
    python3-modules \\
    python3-pexpect \\
    python3-pip \\
    python3-pyyaml \\
    tar \\
    wget \\
    xz \\
    "

MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS += "kernel-modules"
EOF

# Add useful debug info
cat conf/{site,auto}.conf


[ "${CI_JOB_NAME}" = "build-firmware" ] && FIRMWARE="scp-firmware edk2-firmware grub-efi"
[ "${CI_JOB_NAME}" = "build-poky" ] && IMAGE="core-image-minimal"
time bitbake ${FIRMWARE} ${IMAGE}

if [ ${CI_JOB_NAME} = "build-poky" ]; then
  DEPLOY_DIR_IMAGE=$(bitbake -e | grep "^DEPLOY_DIR_IMAGE="| cut -d'=' -f2 | tr -d '"')

  # Prepare files to publish
  rm -f ${DEPLOY_DIR_IMAGE}/*.txt
  find ${DEPLOY_DIR_IMAGE} -type l -delete
  mv ${CI_PROJECT_DIR}/pinned-manifest.xml ${DEPLOY_DIR_IMAGE}
  cat ${DEPLOY_DIR_IMAGE}/pinned-manifest.xml

  # Create MD5SUMS file
  find ${DEPLOY_DIR_IMAGE} -type f | xargs md5sum > MD5SUMS.txt
  sed -i "s|${DEPLOY_DIR_IMAGE}/||" MD5SUMS.txt
  mv MD5SUMS.txt ${DEPLOY_DIR_IMAGE}

  # Move the files to match artifacts paths expectation
  mv ${DEPLOY_DIR_IMAGE}/* ${CI_PROJECT_DIR}/
  cd ${CI_PROJECT_DIR}
  mv core-image-minimal-morello-fvp-*.rootfs.wic core-image-minimal-morello-fvp.wic

  # Pass variables to test job
  echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
  echo "LAVA_TEMPLATE_NAME=fvp-poky.yaml" >> ${CI_PROJECT_DIR}/build.env
  cat ${CI_PROJECT_DIR}/build.env
fi
