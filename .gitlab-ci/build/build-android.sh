#!/bin/sh

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"

TC_DIR=${TC_DIR:-"${HOME}/custom-tc/bin"}
TC_URL=${TC_URL:-}

install_custom_toolchain()
{
  test -d ${TC_DIR} && return 0
  test -z "${TC_URL}" && return 0
  TC="/tmp/morello-clang.tar.xz"
  test -f ${TC} || curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo ${TC} ${TC_URL}
  mkdir -p $(dirname ${TC_DIR})
  tar -xf ${TC} -C $(dirname ${TC_DIR}) --strip-components=1
  export PATH="${TC_DIR}:${PATH}"
  export MORELLO_LLVM_PATH="$(dirname ${TC_DIR})"
  printf "Custom toolchain installed from %s\n" "${TC_URL}"
  printf "MORELLO_LLVM_PATH set to %s\n" "${MORELLO_LLVM_PATH}"
  which clang
  clang --version
}

install_custom_toolchain

FW_DIR="bsp/build-poky/tmp-poky/deploy/images/morello-fvp"
FW_URL="https://git.morello-project.org/morello/morello-ci-pipelines/-/jobs/artifacts/master/raw/${FW_DIR}/"
FW_FILES="bl31-morello.bin bl31-morello.elf bl31.bin bl31.elf grub-efi-bootaa64.efi"
FW_FILES="${FW_FILES} mcp_fw.bin mcp_ramfw_fvp.bin mcp_rom.bin mcp_romfw.bin morello-fvp.dtb"
FW_FILES="${FW_FILES} scp_fw.bin scp_ramfw_fvp.bin scp_rom.bin scp_romfw.bin uefi.bin"

if [ ${1} = "tc-pipeline" ]; then
  mkdir -p ${CI_PROJECT_DIR}/${FW_DIR}
  for fw in ${FW_FILES}; do
    curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo \
      ${CI_PROJECT_DIR}/${FW_DIR}/${fw} "${FW_URL}/${fw}?job=build-firmware"
  done
fi

if [ -z "${ANDROID_GIT_COOKIE}" ]; then
  printf "Skip http.cookiefile\n"
else
  printf "android.googlesource.com\tFALSE\t/\tTRUE\t2147483647\to\t${ANDROID_GIT_COOKIE}\n" > ~/.gitcookies
  chmod 0600 ~/.gitcookies
  git config --global http.cookiefile ~/.gitcookies
  printf "Set http.cookiefile\n"
fi

set -ex

rm -rf .repo/manifests
repo init --depth=1 --no-tags --no-clone-bundle \
  -u https://git.morello-project.org/morello/manifest.git \
  -b ${MANIFEST_BRANCH} -g android,bsp \
  --repo-rev=v2.16

repo selfupdate
repo version

# Set the Android project revision
case "$CI_PROJECT_PATH" in
  morello/kernel/morello-ack)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f2-)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/build-scripts)
    BUILD_SCRIPTS_BRANCH=${BUILD_SCRIPTS_BRANCH:-morello/mainline}
    BUILD_SCRIPTS_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f2-)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${BUILD_SCRIPTS_NAME}\"]/@revision" \
      --value ${BUILD_SCRIPTS_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/device/arm/morello)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/vendor/arm/morello-examples)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/vendor/arm/tools)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/libshim)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/libarchcap)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/art)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f2-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/bionic)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f2-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/build/make)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/build/soong)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/boringssl)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/bzip2)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/compiler-rt)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/conscrypt)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/dnsmasq)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/e2fsprogs)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/freetype)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/googletest)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/icu)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/jemalloc_new)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/kernel-headers)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/libcxx)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/libcxxabi)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/libjpeg-turbo)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/libpng)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/libunwind_llvm)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/llvm)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/lzma)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/pdfium)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/selinux)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/sqlite)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/tcpdump)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/toybox)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/external/zlib)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/frameworks/base)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/frameworks/native)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/hardware/libhardware)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/system/core)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/system/libhidl)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f3-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/system/testing/gtest_extras)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f4-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
  morello/android/platform/system/tools/hidl)
    ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/mainline}"
    ANDROID_PROJECT_NAME=$(echo "${CI_PROJECT_PATH}" | cut -d'/' -f4-)
    xmlstarlet edit --inplace \
      --update "//extend-project[@name=\"${ANDROID_PROJECT_NAME}\"]/@revision" \
      --value ${ANDROID_PROJECT_BRANCH} \
      .repo/manifests/morello-android.xml
  ;;
esac

if [ ! -z ${PROJECT_REFS+x} ]; then
  ./.gitlab-ci/utils/patch_manifest.sh ${PROJECT_REFS} .repo/manifests/morello-android.xml
fi

# Avoid to download +XXG of prebuilt binaries
sed -i '/darwin/d' .repo/manifests/android.xml
sed -i '/windows/d' .repo/manifests/android.xml
xmlstarlet edit --inplace  \
  --update "//remote[@name='aosp']/@fetch" \
  --value "https://android.googlesource.com/a/" \
  .repo/manifests/remotes.xml

repo sync -j8 --quiet --no-clone-bundle
repo manifest -r -o pinned-manifest.xml
cat pinned-manifest.xml

if [ "${1}" = "tc-pipeline" ]; then
  sed -i "s|^CLANG_PREBUILT_BIN=.*|CLANG_PREBUILT_BIN=\"${TC_DIR}\"|" \
    android/build-scripts/platforms/morello/android-kernel/build.config
  sed -i "s|^LINUX_GCC_CROSS_COMPILE_PREBUILTS_BIN=.*|LINUX_GCC_CROSS_COMPILE_PREBUILTS_BIN=\"/usr/bin\"|" \
    android/build-scripts/platforms/morello/android-kernel/build.config
fi

# Include extra packages to the image
# cat << EOF >> android/device/arm/morello/morello_fvp_nano.mk
#
# PRODUCT_PACKAGES += \\
#     bionic-unit-tests \\
#     bionic-unit-tests-static \\
#     binderDriverInterfaceTest \\
#     binderLibTest \\
#     binderSafeInterfaceTest \\
#     binderTextOutputTest \\
#     binderThroughputTest \\
#     binderValueTypeTest \\
#     liblog-unit-tests \\
#     liblog-benchmarks \\
#     logd-unit-tests \\
#     compartment-demo \\
#
# EOF

cd android
# Build Android
./build-scripts/build-android.sh build
# Build tests
export ENVSETUP_NO_COMPLETION=adb:fastboot:asuite
source build/envsetup.sh
if [ -z "${LLVM_PREBUILTS_VERSION}" ]; then
  export LLVM_PREBUILTS_VERSION="clang-local"
  export LLVM_RELEASE_VERSION="$(cd ${CI_PROJECT_DIR}/bsp/build-poky/tmp-poky/sysroots-components/neoversen1/llvm-arm/clang-morello/lib64/clang; echo *)"
fi
if [ ${1} = "tc-pipeline" ]; then
  # On custom toolchain, override the default toolchain path
  TC_BASE_DIR=$(dirname ${TC_DIR})
  export LLVM_RELEASE_VERSION="$(cd ${TC_BASE_DIR}/lib64/clang; echo *)"
fi
lunch morello_fvp_nano-eng
m -j bionic-unit-tests
m -j bionic-unit-tests-static
m -j binderDriverInterfaceTest
m -j binderLibTest
m -j binderSafeInterfaceTest
m -j binderTextOutputTest
m -j binderThroughputTest
m -j binderValueTypeTest
m -j liblog-unit-tests
m -j liblog-benchmarks
m -j logd-unit-tests
for example in $(ls -dR vendor/arm/morello-examples/*/); do
  example=$(echo ${example} | cut -d'/' -f4)
  m -j ${example}
done
m -j userdataimage-nodeps
# Generate Android package
sudo ./build-scripts/build-android.sh package

# Prepare files to publish
cp -a \
  output/morello-nano/*.img \
  output/morello-nano/components/morello/Image \
  output/morello-nano/components/morello/android/*.img \
  ${CI_PROJECT_DIR}/bsp/build-poky/tmp-poky/deploy/images/morello-fvp/*fw.bin \
  ${CI_PROJECT_DIR}/bsp/build-poky/tmp-poky/deploy/images/morello-fvp/uefi.bin \
  ${CI_PROJECT_DIR}
(cd out/target/product/morello; time tar -cJf ${CI_PROJECT_DIR}/userdata.tar.xz data)
# Compress the image
cd ${CI_PROJECT_DIR}
xz grub-android.img
xz userdata.img
# Create SHA256SUMS.txt file
sha256sum *.bin *.img *.xz Image *.xml > SHA256SUMS.txt

# Pass variables to test job
echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
cat ${CI_PROJECT_DIR}/build.env
